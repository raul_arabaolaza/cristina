<?php
if(isset($_POST['email'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "contacto@cristinahornapsicologa.es,udHeEgZKNizGVkvefxDi1T8jV2umy6@api.pushover.net";



    function died($error) {
        // your error code can go here
        echo $error;

        die();
    }

    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['subject']) ||
        !isset($_POST['content'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }

    $first_name = utf8_decode($_POST['name']); // required
    $email_from = utf8_decode($_POST['email']); // required
    $content = utf8_decode($_POST['content']); // required
    $subject= utf8_decode($_POST['subject']);
    $email_subject = "Formulario Web: ".$subject;

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }

  if(strlen($first_name) < 2) {
    $error_message .= 'The Name you entered does not appear to be valid.<br />';
  }

  if(strlen($content) < 10) {
    $error_message .= 'The Comments you entered do not appear to be valid.<br />';
  }

  if(strlen($subject) < 2) {
      $error_message .= 'The subject you entered do not appear to be valid.<br />';
    }
  if(strlen($error_message) > 0) {
    died($error_message);
  }
    $email_message = "Form details below.\n\n";

    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }

    $email_message .= "Nombre: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Consulta: ".clean_string($content)."\n";


// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n".
'Content-Type: text/html; charset="iso-8859-1"'."\r\n".
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);
?>
OK
<?php
}
?>