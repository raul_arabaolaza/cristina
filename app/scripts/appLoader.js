/**
 * Created with JetBrains WebStorm.
 * User: raul
 * Date: 17/07/13
 * Time: 12:30
 */




var states = [
        //terapias
        {
            "stateName": "terapias",
            "abstract": true,
            "url": "/terapias",
            "templateUrl": "./extensionModules/terapias/views/main.html"
        },
        {
            "stateName": "terapias.main",
            "abstract": false,
            "url": "/main",
            "templateUrl": "./extensionModules/terapias/views/terapias.main.html"
        },
        {
            "stateName": "terapias.counselling",
            "abstract": false,
            "url": "/counselling",
            "templateUrl": "./extensionModules/terapias/views/terapias.counselling.html"
        },
        {
            "stateName": "terapias.online",
            "abstract": false,
            "url": "/online",
            "templateUrl": "./extensionModules/terapias/views/terapias.online.html"
        },
        {
            "stateName": "terapias.psicodiagnostico",
            "abstract": false,
            "url": "/psicodiagnostico",
            "templateUrl": "./extensionModules/terapias/views/terapias.psicodiagnostico.html"
        },
        {
            "stateName": "terapias.psicoterapia",
            "abstract": false,
            "url": "/psicoterapia",
            "templateUrl": "./extensionModules/terapias/views/terapias.psicoterapia.html"
        },
        //contacto
        {
            "stateName": "contacto",
            "abstract": true,
            "url": "/contacto",
            "templateUrl": "./extensionModules/contacto/views/main.html"
        },
        {
            "stateName": "contacto.main",
            "abstract": false,
            "url": "/main",
            "templateUrl": "./extensionModules/contacto/views/contacto.main.html"
        },
        {
            "stateName": "contacto.mapa",
            "abstract": false,
            "url": "/mapa",
            "templateUrl": "./extensionModules/contacto/views/contacto.mapa.html",
            "controller": ['$scope', function ($scope) {
                google.maps.visualRefresh = true;

                //Properties needed to google maps directive
                angular.extend($scope, {

                    position: {
                        coords: {
                            latitude: 43.462032,
                            longitude: -3.809263
                        }
                    },

                    /** the initial center of the map */
                    centerProperty: {
                        latitude: 43.462032,
                        longitude: -3.809263
                    },

                    /** the initial zoom level of the map */
                    zoomProperty: 16,

                    /** list of markers to put in the map */
                    markersProperty: [
                        {
                            latitude: 43.462032,
                            longitude: -3.809263
                        }
                    ]
                });
            }]
        },
        {
            "stateName": "contacto.formulario",
            "abstract": false,
            "url": "/formulario",
            "templateUrl": "./extensionModules/contacto/views/contacto.formulario.html",
            "controller": ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {

                $scope.exito = false;

                $scope.error = false;

                $scope.defaultForm={
                      inputName:"",
                    inputSubject:"",
                    inputContent:"",
                    inputEmail:""
                }

                $scope.isCancelDisabled = function () {
                    return $scope.contactForm.$pristine;
                };

                $scope.clearForm=function(){
                    $scope.contactForm.$setPristine();
                    $scope.form=angular.copy($scope.defaultForm);

                }

                $scope.isSaveDisabled = function () {
                    return $scope.contactForm.$invalid || $scope.contactForm.$pristine
                };

                $scope.send = function () {
                    if ($scope.contactForm.$valid || $scope.contactForm.$dirty) {
                        var data = {
                            name: $scope.form.inputName,
                            subject: $scope.form.inputSubject,
                            content: $scope.form.inputContent,
                            email: $scope.form.inputEmail
                        };
                        data = jQuery.param(data);
                        $http(
                            {
                                method: 'POST',
                                url: 'send_mail_form.php',
                                data: data,
                                headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'}
                            }).
                            success(function (data, status, headers, config) {
                                //Yes yes I know my backend should return a 400 response code
                                if (data.indexOf('OK') != -1) {
                                    $scope.exito = true;
                                    $timeout(dismissAlerts, 5000);
                                    $scope.clearForm();
                                } else {
                                    //This should NEVER happend
                                    console.log('Problema al enviar email, respuesta del servidor ' + data);
                                    scope.error = true;
                                    $timeout(dismissAlerts, 5000);
                                }

                            }).
                            error(function (data, status, headers, config) {
                                $scope.error = true;
                                $timeout(dismissAlerts, 5000);
                            });
                    }
                }

                var dismissAlerts = function () {
                    $scope.error = false;
                    $scope.exito = false;
                }

            }
            ]
        },
//areasIntervencion
        {
            "stateName": "areasIntervencion",
            "abstract": true,
            "url": "/areasIntervencion",
            "templateUrl": "./extensionModules/areasIntervencion/views/main.html"
        }
        ,
        {
            "stateName": "areasIntervencion.main",
            "abstract": false,
            "url": "/main",
            "templateUrl": "./extensionModules/areasIntervencion/views/areasIntervencion.main.html"
        }
        ,
        {
            "stateName": "areasIntervencion.individual",
            "abstract": false,
            "url": "/individual",
            "templateUrl": "./extensionModules/areasIntervencion/views/areasIntervencion.individual.html"
        }
        ,
        {
            "stateName": "areasIntervencion.familiares",
            "abstract": false,
            "url": "/familiares",
            "templateUrl": "./extensionModules/areasIntervencion/views/areasIntervencion.familiares.html"
        }
        ,
        {
            "stateName": "areasIntervencion.pareja",
            "abstract": false,
            "url": "/pareja",
            "templateUrl": "./extensionModules/areasIntervencion/views/areasIntervencion.pareja.html"
        }
        ,
//webs amigas
 /*       {
            "stateName": "websAmigas",
            "abstract": false,
            "url": "/websAmigas/main",
            "templateUrl": "./extensionModules/websAmigas/views/main.html"
        }
        ,*/
//Enlaces de Interes
        {
            "stateName": "enlaces",
            "abstract": false,
            "url": "/enlaces/main",
            "templateUrl": "./extensionModules/enlaces/views/main.html"
        }
        ,
//Blog
        {
            "stateName": "blog_listado",
            "abstract": false,
            "url": "/blog/listado",
            "templateUrl": "./extensionModules/blog/views/listado.html",
            "resolve": {
                posts: ['$http', function ($http) {
                    // $http returns a promise for the url data
                    return $http({method: 'GET', url: '/wordpress/?json=get_posts&page=1&count=4'});
                }]
            },
            "controller": ['$http', '$scope', 'posts', '$timeout', function ($http, $scope, posts, $timeout) {

                setPostsData(posts.data);

                function setPostsData(data) {
                    $scope.total = parseInt(data.count_total);
                    $scope.pages = parseInt(data.pages);
                    $scope.posts = [];
                    $scope.currentPage = parseInt(data.query.page);
                    for (var i = 0; i < data.count; i++) {
                        $scope.posts.push(
                            {"title": data.posts[i].title,
                                "id": data.posts[i].id,
                                "excerpt": data.posts[i].excerpt,
                                //necesito esto para que me reconozca la fecha en firefox y el filtro de angular
                                "date": data.posts[i].date.replace(" ", "T")

                            });
                    }
                    ;
                }


                $scope.getClassForPrevious = function () {
                    if ($scope.currentPage == 1) {
                        return "previous disabled";
                    } else {
                        return "previous";
                    }
                };


                $scope.getClassForNext = function () {
                    if ($scope.currentPage == $scope.pages) {
                        return "next disabled";
                    } else {
                        return "next";
                    }
                };

                $scope.loadNext = function () {
                    if ($scope.currentPage < $scope.pages) {
                        $scope.showLoadError = false;
                        $http({method: 'GET', url: '/wordpress/?json=get_posts&count=4&page=' + ($scope.currentPage + 1)})
                            .success(function (data, status, headers, config) {
                                setPostsData(data);

                            }).
                            error(function (data, status, headers, config) {
                                $scope.showLoadError = true;
                                $timeout(dismissAlerts, 5000);

                            });
                    }

                };

                $scope.loadPrevious = function () {
                    if ($scope.currentPage > 1) {
                        $scope.showLoadError = false;
                        $http({method: 'GET', url: '/wordpress/?json=get_posts&count=4&page=' + ($scope.currentPage - 1)})
                            .success(function (data, status, headers, config) {
                                setPostsData(data);

                            }).
                            error(function (data, status, headers, config) {
                                $scope.showLoadError = true;
                                $timeout(dismissAlerts, 5000);

                            });
                    }

                };

                function dismissAlerts() {
                    $scope.showLoadError = false;
                }
            }
            ]
        }
        ,
        {
            "stateName": "blog_post",
            "abstract": false,
            "url": "/blog/{id:[0-9]{1,15}}", //Sageraooo
            "templateUrl": "./extensionModules/blog/views/post.html",
            "resolve": {
                post: ['$http', '$stateParams', function ($http, $stateParams) {
                    // $http returns a promise for the url data
                    return $http({method: 'GET', url: '/wordpress/?json=get_post&post_id=' + $stateParams.id});
//                    return $http({method: 'GET', url: '/post.json'});
                }]
            },
            "controller": ['$http', '$scope', 'post', '$timeout', function ($http, $scope, post, $timeout) {
                console.log(post);
                $scope.postContent=post.data.post.content;
                $scope.postDate=post.data.post.date.replace(" ", "T");
                $scope.postTitle=post.data.post.title;
            }]
        }

    ]
    ;

function initializeApp(angular) {
    var application = angular.module('CristinaApp', ['ui.state', 'google-maps','ngSanitize','ngTouch'])
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            //When no route redirect to home
            $urlRouterProvider.when('', '/home');
            // if the path doesn't match any of the urls you configured
            // otherwise will take care of routing the user to the specified url
            $urlRouterProvider.otherwise('/home');

            $stateProvider.state('home', {
                url: '/home',
                templateUrl: './views/main.html',
                controller: ['$scope', function ($scope) {
                    function initializeCarrouselAndDropdowns() {
                        $('.carousel').carousel({
                            interval: false
                        });
                    }
                    $scope.$on('$viewContentLoaded', initializeCarrouselAndDropdowns);
                }]
            });
            for (var i = 0; i < states.length; i++) {
                var state = states[i];
                $stateProvider.state(state.stateName, {url: state.url, abstract: state.abstract, templateUrl: state.templateUrl, controller: state.controller, resolve: state.resolve});
            }
        }]).run(['$rootScope', function ($rootScope) {
            $rootScope.navBarCollapsed = false;
        }]).config(['$httpProvider', function ($httpProvider) {
            $httpProvider.interceptors.push('loadingStatusInterceptor');
        }]).directive('loadingStatusMessage', ['$animate', function ($animate) {
            return {
                link: function ($scope, $element, attrs) {
                    var show = function () {
                        $animate.removeClass($element, 'ng-hide');

                    };
                    var hide = function () {
                        $animate.addClass($element, 'ng-hide');
                    };
                    $scope.$on('loadingStatusActive', show);
                    $scope.$on('loadingStatusInactive', hide);
                    hide();
                }
            };
        }]).factory('loadingStatusInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
            var activeRequests = 0;
            var started = function () {
                if (activeRequests == 0) {
                    $rootScope.$broadcast('loadingStatusActive');
                }
                activeRequests++;
            };
            var ended = function () {
                activeRequests--;
                if (activeRequests == 0) {
                    $rootScope.$broadcast('loadingStatusInactive');
                }
            };
            var error = function () {
                $rootScope.$broadcast('loadingError');
            }
            return {
                request: function (config) {
                    started();
                    return config || $q.when(config);
                },
                response: function (response) {
                    ended();
                    return response || $q.when(response);
                },
                responseError: function (rejection) {
                    ended();
                    error();
                    return $q.reject(rejection);
                }
            };
        }]).directive('loadingErrorMessage', ['$animate', '$timeout', function ($animate, $timeout) {
            return {
                link: function ($scope, $element, attrs) {
                    var show = function () {
                        $animate.removeClass($element, 'ng-hide');
                        $timeout(hide, 5000);

                    };
                    var hide = function () {
                        $animate.addClass($element, 'ng-hide');
                    };
                    $scope.$on('loadingError', show);

                    hide();
                }
            };
        }]);
    //Now bootstrap application


    angular.bootstrap(document, ['CristinaApp']);


};


initializeApp(angular);
